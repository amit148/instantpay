<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use App\Models\User;
use Session;

class PassportAuthController extends BaseController
{

    /* Api user  Registration */

    public function register(Request $request)
    {
       $rules = array(
           'name'        => 'required|string|min:4|max:255',
           'email'       => 'required|string|email|max:255|unique:users',
           'password'    => 'required|min:8',
       );

       $validation = Validator::make($request->all(), $rules);

       if($validation->fails())
       {
        $errors = $validation->errors();
        $outerror = array();
        foreach($rules as $key => $value) 
        {
          if($errors->has($key))
          {
            $outerror[]= array($key=>$errors->first($key));
        }else{
           $outerror[]= array($key=>'');
       }
   }

   return $this->sendError("Please Enter valid data",$outerror,200);
}

$user = User::create([
    'name'     => $request->name,
    'email'    => $request->email,
    'password' => bcrypt($request->password)
]);

$success['token']             =  $user->createToken('LaravelAuthApp')->accessToken;
$success['name']              =  $user->name;
$success['id']                =  $user->id;
return $this->sendResponse($success,"User Registered Successfully",200);

}

/* Api user Login */

public function login(Request $request)
{

    $rules = array(
       'email'       => 'required|string|email|max:255|exists:users,email',
       'password'    => 'required|string|max:255',
   );

    $validation = Validator::make($request->all(), $rules);

    if($validation->fails())
    {
        $errors = $validation->errors();
        $outerror = array();
        foreach($rules as $key => $value) 
        {
          if($errors->has($key))
          {
            $outerror[]= array($key=>$errors->first($key));
        }else{
           $outerror[]= array($key=>'');
       }
   }

   return $this->sendError('Please Enter valid data',$outerror,200);
}
$data = [
    'email' => $request->email,
    'password' => $request->password
];
if (auth()->attempt($data)) {
    $user             = auth()->user();
    $success['token'] =  $user->createToken('LaravelAuthApp')->accessToken;
    $success['id']    =  $user->id;
    $success['name']  =  $user->name;
    $success['email'] =  $user->email;
    return $this->sendResponse($success,"User Logged in Successfully",200);
} else {
    return $this->sendError('These credentials do not match our records','',401);
}
}   


public function AllUsers(Request $request)
{
 $udata= User::get(['id','name','email']);
 return $this->sendResponse($udata,"User Data retrieved Successfully",200);
}






}