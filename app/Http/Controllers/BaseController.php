<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;


class BaseController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result, $message,$code = 200)
    {
    	$response = [
    		'success' => true,
    		'message' => $message,
    		'data'    => $result,
    	];
    	return response()->json($response, $code);
    }
    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
    	$response = [
    		'success' => false,
    		'message' => $error,
    	];
    	if(!empty($errorMessages)){
    		$response['data'] = $errorMessages;
    	}else{
    		$response['data'] = null;
    	}
    	return response()->json($response, $code);
    }

}