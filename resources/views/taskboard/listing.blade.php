  <!--Main-->
  <main class="bg-white-500 flex-1 p-3 overflow-hidden">
    <div class="flex flex-col">
      <!--Grid Form-->
      <div class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">
        <div class="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
          <div class="bg-gray-200 px-2 py-3 border-solid border-gray-200 border-b">
            Manage Task Board
          </div>
          <div class="p-3">
           <a href="{{url('user/task-board/add')}}"> <button class="bg-blue-500 hover:bg-blue-800 text-white font-bold py-2 px-4 rounded-full">
            New Add
          </button></a>

        </div>
        <div class="p-3">
          <table class="table-responsive w-full rounded">
            <thead>
              <tr>
                <th class="border w-1/4 px-4 py-2">User Name</th>
                <th class="border w-1/6 px-4 py-2">Title</th>
                <th class="border w-1/6 px-4 py-2">Description</th>
                <th class="border w-1/6 px-4 py-2">Date</th>
                <th class="border w-1/7 px-4 py-2">Status</th>
                <th class="border w-1/5 px-4 py-2">Actions</th>
              </tr>
            </thead>
            <tbody>
              @foreach($results as $vals)
              <tr>
                <td class="border px-4 py-2">{{Auth::user()->name}}</td>
                <td class="border px-4 py-2">{{$vals->title}}</td>
                <td class="border px-4 py-2">{{$vals->description}}</td>
                <td class="border px-4 py-2">{{ (!empty($vals->updated_at) ? date('M j, Y',strtotime($vals->updated_at)) : date('M j, Y',strtotime($vals->created_at))) ?? '' }}</td>
                @if($vals->status==0)
                <td title="disable" class="border px-4 py-2">
                  <i class="fas fa-times text-red-500 mx-2"></i>
                </td>
                @endif
                @if($vals->status==1)
                <td title="enable"  class="border px-4 py-2">
                  <i class="fas fa-check text-green-500 mx-2"></i>
                </td>
                @endif
                <td class="border px-4 py-2">
                  <a href="{{url('user/task-board/edit',base64_encode($vals->id))}}" class="bg-teal-300 cursor-pointer rounded p-1 mx-1 text-white">
                    <i class="fas fa-edit"></i></a>
                    <a href="{{url('user/task-board/delete',base64_encode($vals->id))}}" class="bg-teal-300 cursor-pointer rounded p-1 mx-1 text-red-500">
                      <i class="fas fa-trash"></i>
                    </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!--/Grid Form-->
    </div>
  </main>
  <!--/Main-->

  s