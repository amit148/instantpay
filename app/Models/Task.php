<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
	use HasFactory;
	use SoftDeletes;
	
	protected $table = 'task';
	protected $dates = ['deleted_at'];

	protected $fillable = ['title','description','board_id','id','status'];
	
	public $timestamps = false;

	public function taskboard()
	{
		return $this->belongsTo(TaskBoard::class, 'board_id','id');
	}
	
}
