<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\PassportAuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TaskController;


Route::get('clear-cache', function () {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('view:clear');
    Session::flash('success', 'All Clear');
    echo "DONE";
});

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::view('user/register','register');
Route::view('user/login','login');

Auth::routes();

Route::get('dashboard', [HomeController::class, 'Dashboard']);
Route::get('home', [HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'user'], function ()
{
    Route::get('manage/task-board', [HomeController::class, 'TaskBoard']);
    Route::any('task-board/add',[HomeController::class, 'AddTaskBoard']);
    Route::get('task-board/edit/{id}',[HomeController::class, 'EditTaskBoard']);
    Route::get('task-board/delete/{id}',[HomeController::class, 'DeleteTaskBoard']);
    Route::get('task-board/publish/{id}',[HomeController::class, 'PublishTaskBoard']);
    Route::get('task-board/unpublish/{id}',[HomeController::class, 'UnpublishTaskBoard']);

});


Route::group(['prefix' => 'user'], function ()
{
    Route::get('manage/task', [HomeController::class, 'Task']);
    Route::any('task/add',[HomeController::class, 'AddTask']);
    Route::get('task/edit/{id}',[HomeController::class, 'EditTask']);
    Route::get('task/delete/{id}',[HomeController::class, 'DeleteTask']);
    Route::get('task/publish/{id}',[HomeController::class, 'PublishTask']);
    Route::get('task/unpublish/{id}',[HomeController::class, 'UnpublishTask']);

});
