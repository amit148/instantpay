<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Css -->
    <link rel="stylesheet" href="{{asset('public/assets/dist/styles.css')}}">
    <link rel="stylesheet" href="{{asset('public/assets/dist/all.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">
    <!-- Alert -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
    crossorigin="anonymous">
    <style>
    .login{
      background: url({{url('public/assets/dist/images/login-new.jpeg')}})
  }
</style>
<title>User | Register</title>
</head>
<body class="h-screen font-sans login bg-cover">
    <div class="container mx-auto h-full flex flex-1 justify-center items-center">
        <div class="w-full max-w-lg">
            <div class="leading-loose">
                <form class="max-w-xl m-4 p-10 bg-white rounded shadow-xl" method="POST" action="{{ route('register') }}">
                    @csrf
                    <p class="text-gray-800 font-medium">Register</p>
                    <div class="">
                        <label class="block text-sm text-gray-00" for="name">Name</label>
                        <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded @error('name') is-invalid @enderror" id="name" name="name" type="text" placeholder="Your Name" value="{{ old('name') }}" required autocomplete="name" autofocus aria-label="Name">

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="mt-2">
                        <label class="block text-sm text-gray-600" for="email">Email</label>
                        <input class="w-full px-5  py-4 text-gray-700 bg-gray-200 rounded @error('email') is-invalid @enderror" id="email" name="email" type="text" value="{{ old('email') }}" required  placeholder="Your Email" aria-label="Email">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="mt-2">
                        <label class=" block text-sm text-gray-600" for="password">Password</label>
                        <input class="w-full px-2 py-2 text-gray-700 bg-gray-200 rounded @error('password') is-invalid @enderror" id="password" name="password" type="password"   autocomplete="new-password" placeholder="password" aria-label="password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="mt-2">
                        <label class="hidden text-sm block text-gray-600" for="password-confirm">Confirm Password</label>
                        <input class="w-full px-2 py-2 text-gray-700 bg-gray-200 rounded" id="password-confirm" name="password_confirmation" type="password" required autocomplete="new-password" placeholder="confirm password">
                    </div>
                    <div class="mt-4">
                        <button class="px-4 py-1 text-white font-light tracking-wider bg-gray-900 rounded" type="submit">Register</button>
                    </div>
                    <a class="inline-block right-0 align-baseline font-bold text-sm text-500 hover:text-blue-800" href="{{url('user/login')}}">
                        Already have an account ?
                    </a>
                </form>
            </div>
        </div>
    </div>

</body>
</html>