<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskBoard extends Model
{
	use HasFactory;
	use SoftDeletes;
	
	protected $table = 'taskboard';
	protected $dates = ['deleted_at'];
	
	protected $fillable = ['title','description','id','status'];

	public $timestamps = false;

	
	public function task()
	{
		return $this->hasMany(Task::class,'board_id','id');
	}
}
