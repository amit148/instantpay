<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PassportAuthController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\TaskBoardController;



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('task', TaskController::class);
Route::apiResource('task-board', TaskBoardController::class);


Route::any('register', [PassportAuthController::class, 'register']);
Route::post('login', [PassportAuthController::class, 'login']);
Route::any('allusers', [PassportAuthController::class, 'AllUsers']);
