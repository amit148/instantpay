<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use App\Models\User;
use App\Models\TaskBoard;
use App\Models\Task;
use Session;
use Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect()->action([HomeController::class,'Dashboard'])->with('success','User Logged in Successfully');
    }

    public function Dashboard(Request $request)
    {
     $data['content'] ='dashboard'; 
     return view('includes.content',compact('data'))->with(['title'=>'Dashboard']);
 }


 /* TaskBoard */

 public function TaskBoard(Request $request)
 {
    $results = TaskBoard::all();
    $data['content'] ='taskboard.listing'; 
    return view('includes.content',compact('data'))->with(['title'=>'Task Board','results'=>$results]);
}

public function AddTaskBoard(Request $request)
{
    if ($request->isMethod('post'))
    {
        $formdata['user_id']     = Auth::user()->id;
        $formdata['title']       = $request->title;         
        $formdata['description'] = $request->description;
        $formdata['status']      = $request->status; 

        if (!empty($request->id))
        {
            $formdata['updated_at'] = date('Y-m-d H:i:s');
            TaskBoard::whereId($request->id)->update($formdata);
            Session::flash('success','Updated Successfully');   
        }else{
            $formdata['created_at'] = date('Y-m-d H:i:s');
            TaskBoard::insert($formdata);
            Session::flash('success','Created Successfully');
        }
        return Redirect::action([HomeController::class,'TaskBoard']);
    }

    $data['content'] ='taskboard.add'; 
    return view('includes.content',compact('data'))->with(['title'=>'Add']);

}

public function EditTaskBoard(Request $request,$id)
{
    $id = base64_decode($id); 
    $result = TaskBoard::whereId($id)->first();
    $data['content'] ='taskboard.add'; 
    return view('includes.content',compact('data'))->with(['title'=>'Update','result'=>$result]);
}


public function PublishTaskBoard(Request $request,$id)
{
    $id = base64_decode($id);
    $inputdata['status'] = 1;
    TaskBoard::whereId($id)->update($inputdata);
    Session::flash('success', "Enable");
    return Redirect::back();
}

public function UnpublishTaskBoard(Request $request,$id)
{
    $id = base64_decode($id);
    $inputdata['status'] = 0;
    TaskBoard::whereId($id)->update($inputdata);
    Session::flash('error', "Disable");
    return Redirect::back();
}

public function DeleteTaskBoard(Request $request,$id='')
{
 $data =  TaskBoard::where('id',base64_decode($id))->first();
 $data->delete();
 $data->task()->delete();
 return back()->with('success','Deleted successfully');
}


/* Task */

public function Task(Request $request)
{
    $results = Task::all();
    $data['content'] ='task.listing'; 
    return view('includes.content',compact('data'))->with(['title'=>'Task','results'=>$results]);
}

public function AddTask(Request $request)
{
    if ($request->isMethod('post'))
    {
        $formdata['user_id']     = Auth::user()->id;
        $formdata['board_id']    = $request->board;
        $formdata['title']       = $request->title;         
        $formdata['description'] = $request->description;
        $formdata['status']      = $request->status; 

        if (!empty($request->id))
        {
            $formdata['updated_at'] = date('Y-m-d H:i:s');
            Task::whereId($request->id)->update($formdata);
            Session::flash('success','Updated Successfully');   
        }else{
            $formdata['created_at'] = date('Y-m-d H:i:s');
            Task::insert($formdata);
            Session::flash('success','Created Successfully');
        }
        return Redirect::action([HomeController::class,'Task']);
    }
    $boards = TaskBoard::whereStatus(1)->get();
    $data['content'] ='task.add'; 
    return view('includes.content',compact('data'))->with(['title'=>'Add','boards'=>$boards]);

}

public function EditTask(Request $request,$id)
{
    $id = base64_decode($id); 
    $result = Task::whereId($id)->first();
    $boards = TaskBoard::whereStatus(1)->get();
    $data['content'] ='task.add'; 
    return view('includes.content',compact('data'))->with(['title'=>'Update','result'=>$result,'boards'=>$boards]);
}


public function PublishTask(Request $request,$id)
{
    $id = base64_decode($id);
    $inputdata['status'] = 1;
    Task::whereId($id)->update($inputdata);
    Session::flash('success', "Enable");
    return Redirect::back();
}

public function UnpublishTask(Request $request,$id)
{
    $id = base64_decode($id);
    $inputdata['status'] = 0;
    Task::whereId($id)->update($inputdata);
    Session::flash('error', "Disable");
    return Redirect::back();
}

public function DeleteTask(Request $request,$id='')
{
    Task::where('id',base64_decode($id))->delete();
    return back()->with('success','Deleted successfully');
}

}
