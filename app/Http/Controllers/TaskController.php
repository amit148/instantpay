<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use App\Models\User;
use App\Models\TaskBoard;
use App\Models\Task;
use Session;
use Auth;

class TaskController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     $data = Task::whereStatus(1)->get();
     return $this->sendResponse($data,"Task data retrieved Successfully",200);

 }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $rules = array(
        'title'       => 'required|string|max:255|unique:task,title',
        'board_name'  => 'required|numeric',
        'description' => 'sometimes|required|string|max:255',
    );

       $validation = Validator::make($request->all(), $rules);

       if($validation->fails())
       {
        $errors = $validation->errors();
        $outerror = array();
        foreach($rules as $key => $value) 
        {
          if($errors->has($key))
          {
            $outerror[]= array($key=>$errors->first($key));
        }else{
         $outerror[]= array($key=>'');
     }
 }

 return $this->sendError("Please Enter valid data",$outerror,200);
}

$data = new Task([
    'title'       => $request->title,
    'description' => $request->description,
    'board_id'    => $request->board_name,
    'status'      => 1,
    'created_at'  => date('Y-m-d H:i:s')
]);
$data->save();
return $this->sendResponse($data,"Task Created Successfully",200);

}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Task::find($id);
        if (is_null($data)) {
         return $this->sendError("No records found",'',200);
     }
     return $this->sendResponse($data,"Task data retrieved Successfully",200);

 }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $data = Task::find($id);
 }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id='')
    {
        // print_r($request->all());die();

        $rules = array(
            'title'       => 'required|string|max:255|unique:task,title',
            'board_name'  => 'required|numeric',
            'description' => 'required|string|max:255',
        );

        $validation = Validator::make($request->all(), $rules);

        if($validation->fails())
        {
            $errors = $validation->errors();
            $outerror = array();
            foreach($rules as $key => $value) 
            {
              if($errors->has($key))
              {
                $outerror[]= array($key=>$errors->first($key));
            }else{
             $outerror[]= array($key=>'');
         }
     }

     return $this->sendError("Please Enter valid data",$outerror,200);
 }

 $data              = Task::find($id);
 $data->title       = $request->get('title');
 $data->description = $request->get('description');
 $data->board_id    = $request->get('board');
 $data->updated_at  = date('Y-m-d H:i:s');
 $data->save();
 return $this->sendResponse($data,"Task Updated Successfully",200);

}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Task::find($id);
        $data->delete();
        return $this->sendResponse($data,"Task Deleted Successfully",200);

    }
}
