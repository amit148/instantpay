 <!--Main-->
 <main class="bg-white-500 flex-1 p-3 overflow-hidden">
    <div class="flex flex-col">
        <div class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">
            <div class="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
                <div class="bg-gray-200 px-2 py-3 border-solid border-gray-200 border-b">
                   @if(!empty($result->id)) Update @else Add  @endif Task Board
               </div>
               <div class="p-3">
                <form method="POST" action="{{url('user/task-board/add')}}" class="w-full">
                    @csrf
                    <input type="hidden" value="{{$result->id ?? ''}}" name="id" />
                    <div class="flex flex-wrap -mx-3 mb-6">
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1" for="title">
                             Title
                         </label>
                         <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white-500" id="title" value="{{$result->title ?? ''}}" name="title" type="text" required="" placeholder="Title">

                     </div>                    
                 </div>
                 <div class="flex flex-wrap -mx-3 mb-6">
                     <div class="w-full md:w-1/2 px-3">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1" for="description">
                            Description
                        </label>
                        <textarea class="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600" id="description" name="description"  placeholder="Description" required />{{$result->description ?? ''}}</textarea>
                    </div>
                </div>

                <div class="flex flex-wrap -mx-3 mb-6">
                 <div class="w-full md:w-1/2 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1" for="description">
                        Status
                    </label>
                    <select name="status" required="" class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey" id="">
                       @if(@$result->status==1)
                       <option value="1" selected="selected">Enable</option>
                       <option value="0">Disable</option>
                       @else
                       <option value="0" selected="selected">Disable</option>
                       <option value="1">Enable</option>
                       @endif
                   </select>
               </div>
           </div>
           <div class="p-3">
             @if(!empty($result->id))
             <button type="submit" class="bg-blue-500 hover:bg-blue-800 text-white font-bold py-2 px-4 rounded-full">Edit</button>
             @else
             <button type="submit" class="bg-blue-500 hover:bg-blue-800 text-white font-bold py-2 px-4 rounded-full">Submit</button>
             @endif
             <button class="bg-gray-500 hover:bg-blue-800 text-white font-bold py-2 px-4 rounded-full" type="button" onclick="history.back()">Back</button>
         </div>
     </form>
 </div>
</div>
</div>
</div>
</main>